package School;

import org.apache.log4j.*;
import org.apache.log4j.spi.LoggerFactory;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Demo {
    private static Logger logger = LogManager.getLogger("main logger");

    public static void main(String[] args) throws IOException {
        final String format = "%d{yyyy-MM-dd HH:mm:ss,SSS} [%t] %p - %m%n";
        PatternLayout pattern = new PatternLayout(format);
        addConsoleAppender(logger, pattern);
        addFileAppender(logger, pattern);
        demoPeople();
    }

    private static void addFileAppender(Logger logger, PatternLayout pattern) throws IOException {
        final String logFile = "C:\\Mano\\2020 JAVA\\projects\\eschool3\\log.txt";
        FileAppender fileAppender = new FileAppender(pattern, logFile);
        logger.addAppender(fileAppender);
    }

    private static void addConsoleAppender(Logger logger, Layout layout) {
        ConsoleAppender consoleAppender = new ConsoleAppender(layout);
        logger.addAppender(consoleAppender);
    }

    public static void demoPeople() {
        logger.info("start logger");
        Person michael = new Person("Michael", "Jackson", LocalDate.now().minusYears(20), 70, 1.83f, Gender.MALE);
        Person gabriel = new Person("Gabriel", "Jackson", LocalDate.now().minusYears(23), 70, 1.83f, Gender.OTHER);
        Person john = new Person("John", "Jackson", LocalDate.now().minusYears(13), 120, 1.83f, Gender.FEMALE);
        Person viktorija = new Person("Viktorija", "Kurauskiene", LocalDate.parse("1984-12-17"), 64, 1.63f, Gender.FEMALE);

        Person[] people = {michael, gabriel, john, viktorija};
        Stream<Person> peopleStream = Arrays.stream(people);
        peopleStream
                .filter(p -> p.getAge() >= Person.ADULT_AGE)
                .forEach(p -> {
                    System.out.println(p);
                    logger.info("Person is adult: ");
                    logger.info(p);
                });
        //School.Person[] adultsArray = adultsStream.toArray(School.Person[] :: new);
        logger.info("end logger");
    }
}