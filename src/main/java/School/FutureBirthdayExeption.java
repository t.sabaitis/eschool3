package School;

import java.time.LocalDate;

public class FutureBirthdayExeption extends RuntimeException{
    public FutureBirthdayExeption(LocalDate futureDate, String argumentName) {
        super(argumentName + " dateTime cannot be future. Was " + futureDate);
    }
}
