package School;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import java.time.LocalDate;
import static org.assertj.core.api.Assertions.*;
public class PersonTests {
    @Test
    void getAge_when_person_bornToday_returns_0(){
        // Arrange
        Person person = new Person("a", "a", LocalDate.now(), 0, 1, Gender.MALE);
        // Act
        int age = person.getAge();
        // Assert
        assertThat(age).isEqualTo(0);
    }
    @ParameterizedTest(name = "\"{0}\"- throws IllegalArgumentException")
    @ValueSource(strings = {"", " "})
    @NullSource
    void new_given_nullFirstName_throws_IllegalArgumentException(String firstName){
        // Act
        Throwable thrown =  catchThrowable(() -> buildPersonWithFirstName(firstName));
        // Assert
        assertThat(thrown)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("firstName");
    }
    private static Person buildPersonWithFirstName(String firstName){
        return new Person(firstName, "a", LocalDate.now(), 1, 1, Gender.MALE);
    }


    @Test
    void getWeight_when_weight_bellow_zero() {
        // Arrange
        Person person = buildPersonWithNegativeWeight(-1);
        // Act
        float weight = person.getWeight();
        // Assert
        assertThat(weight).isLessThan(0);
    }

    private static Person buildPersonWithNegativeWeight(float weight){
        return new Person("a", "a", LocalDate.now(), weight, 1, Gender.MALE);
    }

    @Test
    void getWeight_when_height_bellow_zero() {
        // Arrange
        Person person = buildPersonWithNegativeHeight(-1);
        // Act
        float weight = person.getHeight();
        // Assert
        assertThat(weight).isLessThan(0);
    }

    private static Person buildPersonWithNegativeHeight(float height){
        return new Person("a", "a", LocalDate.now(), 1, height, Gender.MALE);
    }

    @Test
    void getAge_when_age_is_in_the_future() {
        // Arrange
        Person person = buildPersonWithNegativeAge();
        // Act
        float age = person.getAge();
        // Assert
        assertThat(age).isLessThan(0);
    }

/*    @Test
    void new_given_birthdate_is_in_the_future_throws_IllegalArgumentException() {
        // Act
        LocalDate birthDate = LocalDate.now().plusDays(1);
        Throwable thrown = catchThrowable(() -> buildPersonWithFirstName("A", "B", birthDate));
        // Assert
        assertThat(thrown)
                .isInstanceOf(FutureBirthdayException.class)
                .hasMessage("Birthday cannot be future. Was %s", birthDate);
    }*/

    private static Person buildPersonWithNegativeAge(){
        return new Person("a", "a", LocalDate.now().plusDays(1), 1, 1, Gender.MALE);
    }



/*    void get_person_toString_when_firstName_letter_is_lower_case() {

    }

    void get_person_toString_when_lastName_letter_is_lower_case() {

    }



    void getAge_and_person_if_she_he_is_adult() {

    }*/
}